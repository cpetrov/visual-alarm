let buffer = [];
let isDebug = true;
let consecutiveAlarmCounter = 0;
let consecutiveNonAlarmCounter = 0;

(async () => {
  showNoAlarmUi();
  const model = await tf.loadLayersModel('./model.json');
  // Visualization based on https://github.com/mdn/voice-change-o-matic/blob/gh-pages/scripts/app.js#L123-L167
  const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  const analyser = audioCtx.createAnalyser();
  const stream = await navigator.mediaDevices.getUserMedia({audio: true});
  source = audioCtx.createMediaStreamSource(stream);
  source.connect(analyser);
  analyser.fftSize = 64;
  const frequencyBinsCount = analyser.frequencyBinCount;
  const frequencyBins = new Uint8Array(frequencyBinsCount);
  var canvas = document.querySelector('.visualizer');
  canvas.height = window.innerHeight;
  canvas.width = window.innerWidth;
  var canvasCtx = canvas.getContext('2d');
  function sampleAndDraw() {
    setTimeout(sampleAndDraw, 1000/30); // 30 Hz
    analyser.getByteFrequencyData(frequencyBins);
    buffer.push(Array.from(frequencyBins).slice(0, 12));
    if (buffer.length == 60) {
      const prediction = model.predict(tf.tensor3d([buffer], [1, 60, 12])).dataSync()[0];
      console.log('prediction: ', prediction);
      const isAlarm = prediction < 0.5;
      if (isAlarm) {
        console.log('consecutive alarms: ', consecutiveAlarmCounter + 1);
        if (++consecutiveAlarmCounter > 1) {
          consecutiveNonAlarmCounter = 0;
          showAlarmUi();
        }
      } else { 
        console.log('consecutive non-alarms: ', consecutiveNonAlarmCounter + 1);
        if (++consecutiveNonAlarmCounter > 2) {
          consecutiveAlarmCounter = 0;
          showNoAlarmUi();
        }
      }
      buffer = [];
    }
    if (isDebug) {
      canvasCtx.fillStyle = 'rgb(0, 0, 0)';
      canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
      const barWidth = canvas.width / frequencyBinsCount;
      for (const [i, frequencyBin] of frequencyBins.entries()) {
        canvasCtx.fillStyle = 'rgb(' + frequencyBin + ',50,50)';
        canvasCtx.fillRect(i*(barWidth+1), canvas.height - frequencyBin / 255 * canvas.height, barWidth, frequencyBin / 255 * canvas.height);
      }
    }
  }
  sampleAndDraw();

  function showAlarmUi() {
    document.querySelector('.ok').style.display = 'none';
    document.querySelector('.alarm').style.display = 'flex';
  }

  function showNoAlarmUi() {
    document.querySelector('.ok').style.display = 'flex';
    document.querySelector('.alarm').style.display = 'none';
  }
})();