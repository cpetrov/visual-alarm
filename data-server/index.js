const express = require('express');
const https = require('https');
const cors = require('cors');
const {mkdirpSync, writeJsonSync, readFileSync, readJsonSync, existsSync} = require('fs-extra');

const app = express()
  .use(express.json())
  .use(cors())
  .post('/data', logDataMiddleware);

https.createServer({
  key: readFileSync('../cert/server.key'),
  cert: readFileSync('../cert/server.cert')
}, app).listen(3000, '0.0.0.0', () => {
  console.log('server listening on IPv4 0.0.0.0 port 3000');
});

/**
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
function logDataMiddleware(req, res) {
  mkdirpSync(`data`);
  const filePath = `data/train.json`;
  let data = [];
  if (existsSync(filePath)) data = readJsonSync(filePath);
  console.log(`Received ${req.body.length} samples from server...`);
  const newData = [...data, req.body];
  writeJsonSync(filePath, newData);
  console.log(`All: `, newData.flat().length)
  console.log(`Alarm %: `, newData.flat().filter(data => data.class).length / newData.flat().length);
  res.sendStatus(200);
}
