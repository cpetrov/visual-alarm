let buffer = [];
let lost = 0;
const SERVER_URL = 'https://192.168.1.68:3000';

(async () => {
  // Visualization based on https://github.com/mdn/voice-change-o-matic/blob/gh-pages/scripts/app.js#L123-L167
  const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  const analyser = audioCtx.createAnalyser();
  const stream = await navigator.mediaDevices.getUserMedia({audio: true});
  source = audioCtx.createMediaStreamSource(stream);
  source.connect(analyser);
  analyser.fftSize = 64;
  const frequencyBinsCount = analyser.frequencyBinCount;
  console.log(frequencyBinsCount);
  frequencyBins = new Uint8Array(frequencyBinsCount);
  var canvas = document.querySelector('.visualizer');
  canvas.height = window.innerHeight;
  canvas.width = window.innerWidth;
  var canvasCtx = canvas.getContext('2d');
  function sampleAndDraw() {
    setTimeout(sampleAndDraw, 1000/30); // 30 Hz
    analyser.getByteFrequencyData(frequencyBins);
    buffer.push({
      frequencies: Array.from(frequencyBins), 
      class: document.querySelector('input[type=checkbox]').checked
    });
    if (buffer.length >=100) {
      let bufferLength = buffer.length;
      fetch(`${SERVER_URL}/data`, {
        method: 'post', 
        headers: {'content-type': 'application/json'},
        body: JSON.stringify(buffer)
      }).then(() => {
          console.log(`Sent ${bufferLength} samples to server...`);
        })
        .catch(e => {
          lost += bufferLength;
          console.log(`All lost samples: ${lost}`);
          console.error(e)
        });
      buffer = [];
    }
    canvasCtx.fillStyle = 'rgb(0, 0, 0)';
    canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
    const barWidth = canvas.width / frequencyBinsCount;
    for (const [i, frequencyBin] of frequencyBins.entries()) {
      canvasCtx.fillStyle = 'rgb(' + frequencyBin + ',50,50)';
      canvasCtx.fillRect(i*(barWidth+1), canvas.height - frequencyBin / 255 * canvas.height, barWidth, frequencyBin / 255 * canvas.height);
    }
  }
  sampleAndDraw();
})();