# Visual smoke alarm

A web application that alerts the user visually of an audible smoke alarm.

Requires [Node.js LTS](https://nodejs.org/en/).

It consists of 4 parts:

* `/labeling-ui`

    Web application that sends frequency measurements from the microphone of the user device to `/data-server` along with a label distinguishing smoke alarm sounds from non-smoke-alarm sounds.

    Before serving, adjust the address the `/data-server` has been deployed to in `SERVER_URL` in `labeling-ui/main.js`.

    For serving instructions, see [Serving /labeling-ui and /evaluation-ui](#serving-labeling-ui-and-evaluation-ui).

* `/data-server`
    
    Aggregates data received from `/labeling-ui` in a JSON file.

    To start the server, run `npm start`.

* `/model`
    
    A Jupyter notebook that trains a neural network using data collected by `/data-server`.

    Running `tensorflowjs_converter --input_format keras model.h5 .` will output `group1-shard1of1.bin` and `model.json` that can be used in `/evaluation-ui` with [Tensorflow.js](https://www.tensorflow.org/js).

* `/evaluation-ui`
    
    Captures frequency measurements using the microphone of the user device and uses the trained model from `/model` to detect smoke alarm events. Alerts the user visually when a smoke alarm has been detected.

    For serving instructions, see [Serving /labeling-ui and /evaluation-ui](#serving-labeling-ui-and-evaluation-ui).

## Serving /labeling-ui and /evaluation-ui

`/labeling-ui` and `/evaluation-ui` need to served over HTTPS in order for microphone access to be granted on mobile. For convenience, a self-signed certificate is included in `/cert`. Both projects can be served using the [Node.js](https://nodejs.org/en/) npm module [`http-server`](https://www.npmjs.com/package/http-server):

```sh
npm i -g http-server
cd evaluation-ui # or labeling-ui
hs -S -C ../cert/server.cert -K ../cert/server.key
```